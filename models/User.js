const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({

    firstName: 
    {
        type: String,
        require: [true, 'Name is required']
    },

    lastName: 
    {
        type: String,
        require: [true, 'Last Name is required']
    },

	mobileNo:{
        type: String,
        require: [true, 'Number is required']
    },

    email:{
        type: String,
        require: [true, 'Email is required']
    },

    password:{
        type: String,
        require: [true, 'Product ID is required']
    },
    
    isAdmin:{
        type: Boolean,
        default: false
    }
})

module.exports = mongoose.model("User", userSchema);
