const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderController");
const auth = require('../auth.js');


router.post("/add-order", auth.verify, (req, res)=> {
	const userData = auth.decode(req.headers.authorization);

	orderController.addNewOrder(req.body, {userId: userData.id, isAdmin:userData.isAdmin}).then(resultFromController => res.send(resultFromController));

});


module.exports = router;
