const Product = require("../models/Product");
const User = require("../models/User");

//ADD NEW PRODUCTS

module.exports.addProduct = (reqBody) => {

	let newProduct = new Product({

		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		stocks: reqBody.stocks,
		category: reqBody.category,

	});


	return newProduct.save().then( (product, error) => {

		if(error) {

			return false

		} else {

			return true
		}

	});
};

//RETRIEVE ALL ACTIVE PRODUCTS
module.exports.getAllProduct =()=> {
	return Product.find({}).then(result => {
		return result;
	})
}

//RETRIEVE ALL ACTIVE PRODUCTS
module.exports.getAllActive = () => {
	return Product.find({isActive:true}).then (result => {
		return result;
	})
}

//RETRIEVE SPECIFIC PRODUCT
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result
	})
}


//UPDATES A PRODUCT
module.exports.updateProduct = (reqParams, reqBody, data) => {

    return User.findById(data.id).then(result => {
        console.log(result)

        if(result.isAdmin === true) {

            // FIELDS OF DOCUMENTS TO BE UPDATED
            let updatedProduct = {
                name: reqBody.name,
				description: reqBody.description,
				price: reqBody.price,
				stocks: reqBody.stocks,
				category: reqBody.category,

            };

           
            return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {

               
                if(error) {

                    return false

              
                } else {

                    return true
                }
            })

        } 
        else {

            return false
        }



    })


}

//ARCHIVE PRODUCTS
module.exports.archiveProduct = (data, reqBody) => {

	return Product.findById(data.productId).then(result => {

		if (data.payload === true) {

			let updateActiveField = {
				isActive: reqBody.isActive
			}

			return Product.findByIdAndUpdate(result._id, updateActiveField).then((product, err) => {

				if(err) {
					
					return false
					
				}  
				else {

					return true
				}
			})		 
		} 
		else {
			return false
		} 
	})
}
