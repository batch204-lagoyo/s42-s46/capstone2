const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const port = process.env.PORT || 4000;



const userRoutes = require('./routes/userRoutes')
const productRoutes = require('./routes/productRoutes')
const orderRoutes = require('./routes/orderRoutes')
const app = express();


app.use(cors());
app.use(express.json());



mongoose.connect("mongodb+srv://admin:admin12345@batch204-lagoyo.0azkaog.mongodb.net/capstone2?retryWrites=true&w=majority", 
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }
);

const db = mongoose.connection;
db.on("error", () => console.error.bind(console, "Error"));
db.once("open", () => console.log("Now connected to MongoDB Atlas!"));

// End routes
app.use('/users', userRoutes); 
app.use('/products', productRoutes); 
app.use('/orders', orderRoutes); 



app.listen(port, () => {
	console.log(`API is now online on port ${port}`);
});
